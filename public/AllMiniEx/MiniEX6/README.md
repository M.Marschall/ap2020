https://m.marschall.gitlab.io/-/ap2020/-/jobs/472587562/artifacts/public/AllMiniEx/MiniEX6/index.html
### Objective
- to use a simulation algorithm
- explore the use of objects
- it must be a game
- Get folder of images an put them into a sequence and play them as 12fps animation. Maybe making an animator class.
- Functionality of the object

### Execution
So did not have the time to integrate all of Craig Reynolds flock behavior algorithm , but I used some of it :) I especially discovered the power of vectors in p5.js. It is a really nice way to understand and work with different forces and their impacts.  

I wanted to make a hunting game were you would have different element that would affect the "simulation".

####The birds

The birds will start flying fast if you shoot at them without hitting. On hit i will die and gravity will make it fall to the ground. They are never standing still. The animation are really simple drawings that I have made and saved as PNG files.  

####The aim
The idea was that every time the hunter would  hit a bird, he/she would celebrate with a drink. eventually it would be harder and harder to aim with the gun.

### Thoughts about object oriented programming

Creating classes and object to store values and methods Is a really effective way of managing your program. It is what really enables coding to be a social activity because these classes and object are often already packed with all you need to make them work in your own program.

What one should notice is that these objects are usually very subjective the programmer has selected things for attention and other attributes are ignored.

The shareable code comes with a responsibility to both the author and the user. The user has Remember to stay critical to these to other programmers objects and classes, because they are packed with subjective decisions and ideology. Authors of code should make it as transparent as possible by creating documentation and explanations for how to object is actually behaving.
