class aimGun {
  // So this class is mostly based on simple vector math
  // Remember when reading this that x and y of vectors has nothing to do with
  // position but only length and direction.
  constructor(x, y) {
    this.position = new p5.Vector(0, 0); //Empty vector basically a dot with no direction or length
    this.velocity = new p5.Vector(0, 0);//Empty vector basically a dot with no direction or length
    this.acceleration = new p5.Vector(x, y);//sets the direction and length of the acceleration vector

    this.maxforce = 100; //the maximum force that the object can use to change direction an speed.
    this.maxspeed = 4 * 5; //Maximum speed of the object
    this.reactionDistance = 100; //The distance to the target when it will start to react and slow down.
  }

  //update to the new position, direction and speed.
  update() {
    this.velocity.add(this.acceleration);//add the acceleration to the current velocity (velocity is both speed and directon)
    this.velocity.limit(this.maxspeed);//limits the velocity to the max speed - in math this would be limiting the length of the vector while maintaining the direction.
    this.position.add(this.velocity);//Sets the new position by adding the velocity to the current position. Euler Integration
    this.acceleration.mult(0);//Resetting the acceleration to 0 by multiplying the vector by 0
  }

  //The method just adds the force of the input vector
  applyForce(force) {
    this.acceleration.add(force);//vector math add
  }

  //this is the method that makes the aim seek the target which is the position of the mouse
  seek(target) {
    let desired = p5.Vector.sub(target, this.position);// Create s a vector that points all the way to the desired target (this also gives the ability to calculate the distance towards the target)
    let d = desired.mag(); //Calculates the magnitude which is the length of the vector(distance to the target). It is done by using Pythagoras theorem a^2 + b^2 = c^2
    desired.normalize();// Sets the length of the vector to 1
    if (d < this.reactionDistance) { //If the distance from the aim to the target i less than this.reactionDistance then do this...
      //...set the magnitude according to how close we are.
      let m = map(d, 0, this.reactionDistance, 0, this.maxspeed);// m = length defined i "d" mapped from 0 to reaction distance  - to 0 tod maxspeed
      // could also be written as: let m = d/this.reactionDistance*this.maxpeed;
      //What this means is that every time the program updates and the target it within the desired distance, then
      // it will calculate new value that goes toward 0 but never actually zero

            desired.mult(m);//multiplying the desired vector by the m value.
            //Becuase m is decreasing in value for every iteration that the
            // target is within distance, it will create the effect of slowing down the aim/object
    } else {
      //Otherwise, proceed at maximum speed.
      desired.mult(this.maxspeed);
    }
    let steer = p5.Vector.sub(desired, this.velocity);// Create a vector that is the steering force = desired velocity - the current velocity (as defined by Craig Reynolds)
    steer.limit(this.maxforce); //This limits how strong the acceleration can be by limiting the maximum length/magnitude of the vector
    this.applyForce(steer);//Apply the force of the new vector.
  }

  //Renders the aim
  display() {
    fill(175);
    stroke(0);
    ellipse(this.position.x, this.position.y, 70);

  }

}
