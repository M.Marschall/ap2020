class animatedBirds {
  constructor(positionVector) {
    this.frameArray = [];
    this.position = positionVector;
    this.folderURL;
    this.gravitySpeed = 0;
    this.gravity;
    this.size = [];
    this.hit = false;
    this.moverT;


    this.countUntilChangeOfTarget = 0;
    this.birdLocationTarget = new p5.Vector(random(0,windowWidth),random(0,windowHeight))

    this.isGravityActive = false;

    this.timeAfterHit = 0;

    this.velocity = new p5.Vector(0, 0);//Empty vector basically a dot with no direction or length
    this.acceleration = new p5.Vector(0, 0);//sets the direction and length of the acceleration vector

    this.maxforce = 0.1*5; //the maximum force that the object can use to change direction an speed.
    this.maxspeed = 1; //Maximum speed of the object
    this.reactionDistance = 100; //The distance to the target when it will start to react and slow down.
  }
  /* This was the first version for loading the files.
  I want to make it easier to use so that you do not have to specify how many
  frames but instead just load all images in a specific folder.

  Since javascript is a clientside language - i found it hard to actually get the
  amount of files within a folder. So for now you would have to specify the exact
    number. */

  loadFrames(frameCountNumber, folderURL) {
    let fc = frameCountNumber;
    this.folderURL = folderURL;
    let imagesAreLoaded = false;
    for (let i = 0; i < fc; i++) {
      this.frameArray[i] = loadImage(this.folderURL + 'frame' + i + '.png');
    }
  }
  //The animate function just loops the array by taking tha first image adding
  // it to the back and then remove i from the front of the array.
  animate() {
    if (!this.hit) {
      this.frameArray.push(this.frameArray[0]);
      this.frameArray.shift();
    }
  }
  //Displays the object
  display(sX, sY) {
    imageMode(CENTER);
    image(this.frameArray[0], this.position.x, this.position.y, sX, sY);
  }

  //Everything that has to do with the position of the object like gravity etc;
  updatePosition() {
    this.velocity.add(this.acceleration);//add the acceleration to the current velocity (velocity is both speed and directon)
    this.velocity.limit(this.maxspeed);//limits the velocity to the max speed - in math this would be limiting the length of the vector while maintaining the direction.
    this.position.add(this.velocity);//Sets the new position by adding the velocity to the current position. Euler Integration
    this.acceleration.mult(0);//Resetting the acceleration to 0 by multiplying the vector by 0


    let currentPosition = new p5.Vector(this.position.x, this.position.y);
    if (this.isGravityActive) {

      this.gravity = 0.05;
      this.gravitySpeed = this.gravitySpeed + this.gravity;
      this.position.x = this.position.x
      this.position.y = this.position.y + this.gravitySpeed;

    }
    if (this.hit) {
      this.timeAfterHit++;

      this.onHit(1, 1, 10);
    } else {
      this.countUntilChangeOfTarget++;
      this.seek(this.birdLocationTarget);
        if(this.countUntilChangeOfTarget>30) {
          this.birdLocationTarget.set(random(0,windowWidth),random(0,windowHeight));
          this.countUntilChangeOfTarget = 0;
        }
    }

  }
  applyForce(force) {
    this.acceleration.add(force);//vector math add
  }
  //this is the method that makes the aim seek the target which is the position of the mouse
  seek(target) {
    let desired = p5.Vector.sub(target, this.position);// Create s a vector that points all the way to the desired target (this also gives the ability to calculate the distance towards the target)
    let d = desired.mag(); //Calculates the magnitude which is the length of the vector(distance to the target). It is done by using Pythagoras theorem a^2 + b^2 = c^2
    desired.normalize();// Sets the length of the vector to 1

      desired.mult(this.maxspeed);

    let steer = p5.Vector.sub(desired, this.velocity);// Create a vector that is the steering force = desired velocity - the current velocity (as defined by Craig Reynolds)
    steer.limit(this.maxforce); //This limits how strong the acceleration can be by limiting the maximum length/magnitude of the vector
    this.applyForce(steer);//Apply the force of the new vector.
  }
  onHit(x, y, force) {
    let test = new p5.Vector(x, y);



    test.mult(-1); //the force goes in to opposite direction of the hit.
    test.div(this.timeAfterHit / force);
    this.position.add(test);
  }

}
