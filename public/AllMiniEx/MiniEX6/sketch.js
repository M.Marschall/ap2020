let test;
let frameCountNumber = 0;
let aim;
let targetAim;

let roundsSpend = 0;
let birdsKilled = 0;

let arrayOfBirds = [];
let amountOfBirds = 10;

let howScaredAreTheBirds =1;

function preload() {
  for (let i = 0; i < amountOfBirds; i++) {
    arrayOfBirds[i] = new animatedBirds(new p5.Vector(random(50, windowWidth - 50), random(50, windowHeight - 50)));
    arrayOfBirds[i].loadFrames(8, './AnimTest1/');
    for (let t = 0; t < floor(random(0, 10)); t++) {
      arrayOfBirds[i].animate();
    }
  }

  aim = new aimGun(100, 100);
  targetAim = new p5.Vector();
}

function setup() {
  // put setup code here
  createCanvas(windowWidth, windowHeight);
  print("hello world");

}

function draw() {
  background(230, 50, 132);
  targetAim.set(mouseX, mouseY);
  frameRate(48);
  frameCountNumber++;
  aim.seek(targetAim);
  aim.update();
  aim.display();
  handDrawnAnimation();

  for (let i = 0; i < arrayOfBirds.length; i++) {
    arrayOfBirds[i].display(200, 200);
    arrayOfBirds[i].updatePosition();
    arrayOfBirds[i].maxspeed = howScaredAreTheBirds;
    //ellipse(arrayOfBirds[i].position.x, arrayOfBirds[i].position.y, 100)
    if (arrayOfBirds[i].position.y > windowHeight * 2) {
      if (arrayOfBirds.length) {
        arrayOfBirds.splice(i, 1);
      }
    }
  }
  textSize(32);
  text('Rounds Used: ' + roundsSpend, 10, 30);
  text('Birds Killed: ' + birdsKilled, 10, 90);
  fill(0, 102, 153);

  checkIfMoreBirdsAreNeede();
}

function mousePressed() {
  roundsSpend++;
  for (let i = 0; i < arrayOfBirds.length; i++) {
    howScaredAreTheBirds = howScaredAreTheBirds+25;

    let dist = p5.Vector.dist(arrayOfBirds[i].position, aim.position);
    if (dist < 50) {
      //Shot one bird and scare the other birds and make them go faster


      birdsKilled++;
      arrayOfBirds[i].hit = true;
      arrayOfBirds[i].isGravityActive = true;
      theHunterTookADrinkToCelebrate();

    }

  }

}
function checkIfMoreBirdsAreNeede() {
  if(arrayOfBirds.length<amountOfBirds) {
    for(let i = arrayOfBirds.length; i<amountOfBirds; i++) {
      arrayOfBirds[i] = new animatedBirds(new p5.Vector(random(50, windowWidth - 50), -100));
      arrayOfBirds[i].loadFrames(8, './AnimTest1/');
    }
  }
}

// EVerything in this function is animated at 12 fps
function handDrawnAnimation() {
  if (frameCountNumber > 3) {
    for (let i = 0; i < arrayOfBirds.length; i++) {
      arrayOfBirds[i].animate()
    }
    if(howScaredAreTheBirds>4) {
      howScaredAreTheBirds = howScaredAreTheBirds*0.8;
    }





    frameCountNumber = 0;
  }
}

function theHunterTookADrinkToCelebrate() {
  aim.maxforce = aim.maxforce / 1.5;
}
