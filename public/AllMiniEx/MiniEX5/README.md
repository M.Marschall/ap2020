## The program
The more you smile the higher the salary...

https://m.marschall.gitlab.io/-/ap2020/-/jobs/463786734/artifacts/public/AllMiniEx/MiniEX5/index.html

## MiniEx5 - Thoughts in the process  

So In this MiniEx i did not want to explore any new syntax or libraries. I want to look at source code of MiniEx2 and the use of pre-trained models in face recognition and then try to understand some of the issues that might occur when treating collected data in such a model. I decided to do this because there is a lot I do not know about how a computer learns and how this affects the models that some developers might use without knowing the consequence. When i was doing the MiniEx2 i just downloaded some models without even questioning the making is them and what they represent. I would love to create my own machine learning project at some point, but for now, I thought it would be good to get to know the core concepts of it before "copying" code without knowing the implications.

### A bad machine learning program
I thought to my self - what is one of the worst ways neural networks could be used? And I quickly came up with the idea that we could make a program that decided the salary of a person starting in a new job and then give it an image of the person as an input. Let us call this program something catchy (and a little provocative) like "Face-> Salary".

The way it would work would be by taking data about similar positions in similar companies/institutions, an image of every person and their salary. For this fictive project, we will settle for some supervised machine learning, with automatic feature selections. Our dataset would consist of an image of people having similar positions and their salary.

Everything that follows would be a reflection on this fictive program:

### How do we train a machine?

I thought that it would be important to know the very fundamentals of neural networks to begin with - but that was too hard for me to fully comprehend in detail. So I have decided to only try to explain the parts that are necessary for the point I would like to make.  I am dealing only with the training of the model and not how neural networks work.

 In our fictive machine learning program mentioned above, we would like to make the learning supervised. In the simplest form, this means that there is the labeling of different categories, and in this case, we are going to provide the computer with an image and the correct salary.

It was somewhat hard to fully understand the different learning forms.

https://www.youtube.com/watch?v=cfj6yaYE86U (A 5 min video on Supervised vs Unsupervised learning)
https://www.youtube.com/watch?time_continue=137&v=rbaf-rPN1Ig&feature=emb_logo
Supervised Learning

The feature selection describes the parameters that we want to deal with and we single out certain features to optimize our model. These are usually selected from our knowledge of the domain that we are working within. An example could be a program that decided what fruit was in a picture and we know that the aspect ratio is different depending on the fruit e.g. a banana is long and slim and an apple is as wide as it is high, and therefore selecting aspect ratio could be a good feature for classifying the image. In this case, however, we would like to make this selection automated because we did not want to take the time to be good designers and look into the domain that we are dealing with ;)
There are a lot of different feature-selecting algorithms but I am not going to pretend to know any of them.

So we went with automated features selections for our "Face-> Salary" program and just fed he dataset to our machine. The machine processes the dataset tries to create a model and finally, it selects some features that have a certain significance. I am in NO WAY in any position to be able to explain what happens but it is here that most of my reflections arise.

How do we understand these features that have been selected? and are these features selected in respect to what culture and society we live in or would like to live in?

The first question has to do with the evolvement this model goes through. When we are dealing with an image where to computer recognizes certain features as being important to the outcome then it is really hard to understand them and decipher what they are. It evolves beyond human recognition and that is where it can become a little worrying because of the lack of control.

This leads to the second question about the more ethical implications of the features selection. In Denmark, we do not have equal pay yet between men and women, which we can recognize as true, but also a problem in our society. I would say that in our "Face -> Salary" program, one of the key features that the computer might select without us knowing would be some features that somewhat translates to the gender of the person in the picture. This would mean that if we use our regression model to decide the salary of a person, we might see a jump depending on the gender of the person in the image. We as humans would see this a decision on salary as a problem, but the computer will see it as a definite truth and in no way challenge the status quo.

The "Face -> Salary" program is of course very ridiculous, but none the less this problem have been seen in modern programs. A large case from New Orleans police department can be read here:
https://www.technologyreview.com/s/612957/predictive-policing-algorithms-ai-crime-dirty-data/

A data-mining firm had made a model that should predict the location of the crime, but it was built on falsified data that contained trails of historical racism in the department.

There are so many ways these models can go wrong. Are they representative? do they reflect the truth? and if so, do we then want to keep building upon this "truth"?
The data that search engines and social medias collect about us might be used in making regression, classification and clustering models and is used for targeting ads and content. Still the same issues applies - it might be the right content for displayed for increasing interaction, but it might not be the right content for challenging status quo.
