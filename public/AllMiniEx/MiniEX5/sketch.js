let video;
let interval;
let exprs = {};
let face;
let salary = 0;

let happyValue = 0;
let possibleEmotions = [];

function setup() {

  createCanvas(640, 480);
  background(255);

  video = createCapture(VIDEO);
  video.hide(); // hides the html video element i just created
  interval = setInterval(timeIt, 100); // calls the funktion timeIt every 100ms.

  //loading the face-api.js using ml5.js (machine learning Library for p5.js)
  faceapi.load
  faceapi.loadSsdMobilenetv1Model('./models'); //loads the models for finding a face
  faceapi.loadFaceExpressionModel('./models'); //loads the models for determining face expressions.
}


//i use async function to make sure that the facial detection can be finished before proceeding.
async function howBigIsYourSmile() {


  const myPromise = async () => {
    //i tell the program to wait for the function to finish
    await faceapi.detectSingleFace(video.elt).withFaceExpressions()
      .then((detectionOfExpression) => {

        //this just checks if there is even a face in the picture ;-)
        if (detectionOfExpression == undefined) {
          console.log("No face detected");
        } else {
          face = detectionOfExpression.detection;
          exprs = detectionOfExpression.expressions;
          //i get the index number of the larges value
          happyValue = Object.values(exprs)[1];
          possibleEmotions = Object.keys(exprs);
          //Object.values returns an array[] and max() just returns
          //console.log(Math.max(Object.values(exprs)));
          print(happyValue);


        }

      });

  }
  myPromise();
}

function draw() {
  if(happyValue<0.2 && happyValue<0) {
    salary = 0;
  }
  if(happyValue<0.4 && happyValue>=0.2) {
    salary = 2000;
  }
  if(happyValue<0.6 && happyValue>=0.4) {
    salary = 3000;
  }
  if(happyValue<0.7 && happyValue>=0.6) {
    salary = 4000;
  }
  if(happyValue<0.8 && happyValue>=0.7) {
    salary = 6000;
  }
  if(happyValue<0.9 && happyValue>=0.8) {
    salary = 8000;
  }
  if(happyValue<.95 && happyValue>=0.9) {
    salary = 12000;
  }
  if(happyValue<0.98 && happyValue>=0.95) {
    salary = 18000;
  }
  if(happyValue<0.99 && happyValue>=0.98) {
    salary = 26000;
  }
  if(happyValue<=1 && happyValue>=0.99) {
    salary = 40000;
  }




  background(255, 0, 0);
  image(video, 0, 0, 640, 480); //puts the video onto the canvas
  scale(1);


  fill(50);
    text("Monthly salary: " + salary + "+ DKK", 10, 10, 70, 80);


}

function timeIt() {
  howBigIsYourSmile();
}
