let animator1 = 0;
let animator2 = 0;
let speed = 100;
let linePosY;
let linePosX;

function setup() {
  // put setup code here
  createCanvas(windowWidth,windowHeight);
  linePosY = random(height/2+50,height/2-200);
  linePosX = width;
  frameRate(60);
}
function draw() {
  // put drawing code here
  background(230,50,132);
  fill(0,0,0,30);
  noStroke()
  ellipse(width/2,height/2+27,250,10-random(0,2))

fill(117,196,255);
textSize(32)
text('We are going as fast as we can!',width/2-240,height/2+80);

drawScooter(0);

strokeWeight(8);
stroke(0);

if(animator1*speed< 0-width) {
    linePosY = random(height/2-300,height/2);
    animator1 = 0;
}
if(800+animator2*speed< 0-width) {
    linePosY = random(height/2-300,height/2);
    animator2 = 0;
}

push();
animator1--;
translate(animator1*speed,0);
stroke(0,0,0,40);
line(width-50,linePosY,width+50,linePosY);
pop();

}

  function mousePressed() {
    console.log("X: " + mouseX + " Y: " + mouseY);
  }

function drawScooter(speedDistortion) {
  let distortion = speedDistortion;

  //Wheels
  let rearWheelCenter = [width/2-90-distortion, height/2];
  let frontWheelCenter = [width/2+90+distortion,height/2];

translate(random(0,1),random(0,2));

  noStroke();
  fill(40);
  ellipse(rearWheelCenter[0], rearWheelCenter[1],50,50);
  ellipse(frontWheelCenter[0], frontWheelCenter[1],50,50);
  fill(255);
  ellipse(rearWheelCenter[0], rearWheelCenter[1],35,35);
  ellipse(frontWheelCenter[0], frontWheelCenter[1],35,35);
  fill(200);
  ellipse(rearWheelCenter[0], rearWheelCenter[1],25,25);
  ellipse(frontWheelCenter[0], frontWheelCenter[1],25,25);
  fill(100);
  ellipse(rearWheelCenter[0], rearWheelCenter[1],10,10);
  ellipse(frontWheelCenter[0], frontWheelCenter[1],10,10);


  fill(64,131,179);
  beginShape();
  vertex(rearWheelCenter[0]-40,rearWheelCenter[1]);
  vertex(rearWheelCenter[0]-10, rearWheelCenter[1]-60);
  vertex(rearWheelCenter[0]+80, rearWheelCenter[1]-60);
  vertex(rearWheelCenter[0]+80, rearWheelCenter[1]-50);
  vertex(rearWheelCenter[0]+60, rearWheelCenter[1]-40);
  vertex(rearWheelCenter[0]+80, rearWheelCenter[1]);
  endShape();

  push();
  noFill();
  stroke(64,131,179);
  strokeWeight(15);
  bezier(frontWheelCenter[0]-40,frontWheelCenter[1]-100,frontWheelCenter[0]+20,frontWheelCenter[1],frontWheelCenter[0]+20,frontWheelCenter[1],rearWheelCenter[0],rearWheelCenter[1]);


  pop();

  beginShape();
  vertex(frontWheelCenter[0]-40,frontWheelCenter[1]-100);
  vertex(frontWheelCenter[0]-15,frontWheelCenter[1]-90);
  vertex(frontWheelCenter[0]-15,frontWheelCenter[1]-110);
  endShape();

  fill(200);
  ellipse(frontWheelCenter[0]-40,frontWheelCenter[1]-100,10);
  fill(50);
  ellipse(frontWheelCenter[0]-40,frontWheelCenter[1]-98,10,8);

    fill(64,131,179);
    push();

    arc(frontWheelCenter[0],frontWheelCenter[1], 55, 55, radians(170), radians(-20), CHORD);
    pop();



  fill(117,197,255);
  arc(rearWheelCenter[0],rearWheelCenter[1], 80, 90, radians(170), radians(10), CHORD);
  fill(64,131,179);
  arc(rearWheelCenter[0],rearWheelCenter[1], 80, 80, radians(170), radians(10), CHORD);

  fill(117,197,255);
  rect(rearWheelCenter[0]-40,rearWheelCenter[1],80,10);
  fill(64,131,179);
  rect(rearWheelCenter[0]-40,rearWheelCenter[1]+2,76,8);

  fill(200);
  rect(rearWheelCenter[0]-10, rearWheelCenter[1]-60,92,-11);
  fill(50);
  rect(rearWheelCenter[0]-10, rearWheelCenter[1]-60,92,-10);

  //human

  //Pants'

  push();

  fill(30,62,84);
  beginShape();
  vertex(rearWheelCenter[0]+50,rearWheelCenter[1]-100);
  vertex(rearWheelCenter[0]+10,rearWheelCenter[1]-70);
  vertex(rearWheelCenter[0]+50,rearWheelCenter[1]-70);
  vertex(rearWheelCenter[0]+100,rearWheelCenter[1]-65);
  vertex(rearWheelCenter[0]+130,rearWheelCenter[1]-12);
  vertex(rearWheelCenter[0]+155,rearWheelCenter[1]-20);
  vertex(rearWheelCenter[0]+115,rearWheelCenter[1]-80);
  endShape();

  pop();
  fill(255,199,117);
  beginShape();
  vertex(rearWheelCenter[0]+5,rearWheelCenter[1]-100);
  vertex(rearWheelCenter[0]+10,rearWheelCenter[1]-70);

  vertex(rearWheelCenter[0]+50,rearWheelCenter[1]-100);

  vertex(rearWheelCenter[0]+85,rearWheelCenter[1]-140);
  vertex(rearWheelCenter[0]+65,rearWheelCenter[1]-160);

  endShape();
  stroke(255,199,117);
  strokeWeight(15);
  line(rearWheelCenter[0]+90,rearWheelCenter[1]-100,rearWheelCenter[0]+65,rearWheelCenter[1]-145)
  line(rearWheelCenter[0]+90,rearWheelCenter[1]-100,frontWheelCenter[0]-50,frontWheelCenter[1]-100)

  ellipse(rearWheelCenter[0]+95,rearWheelCenter[1]-170,30);

  fill(196,138,51);
  noStroke();
  arc(rearWheelCenter[0]+95,rearWheelCenter[1]-170, 65, 65, radians(170), radians(10), CHORD);

  rect(rearWheelCenter[0],rearWheelCenter[1]-70,-75,-75);
  fill(117,196,255);
  textSize(32)
  text('Data',rearWheelCenter[0]-70,rearWheelCenter[1]-100);



}
