## Exercise in class

To familiar yourself with different modes of capture, try the following:

Explore the different modes of capture by tinkering with various parameters such as keyCode, as well as other keyboard and mouse events.
Study the tracker points and try to change the position of the Like button that you previously customized in the earlier exercise.
Try to test the boundaries of facial recognition: to what extend can(not) a face be recognized as a face?
Do you know how the face is being modelled? How has facial recognition technology been applied in society at large, and what are some of the issues that arise from this?

It would be worth checking back to Variable Geometry for a reminder of how facial recognition identifies a person's face from its geometry — such as the distance between a person's eyes or size of mouth — to establish a facial signature that is comparable to a standardised database. Not least of the problems is that the database is itself skewed by the ways in which the data has been prepared, in its selection, collection, categorization, classification and cleaning. To what extent does your face meet the standard?
