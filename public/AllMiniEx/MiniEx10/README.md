Individual: https://cm2df7.axshare.com
I find flowchart difficult to manage because it seems like there is almost no standardization of them. I search up some examples but most of the examples i found was not very conceptual but was more about the technical. I believe i can be a good communcative tool but i would think that in our group we have to find common ground before it can be really effective. I have tried to make a conceptual explanation of MiniEx8 https://gitlab.com/Roeskva/ap2020/-/tree/master/public/MiniX8.

Groupwork
![](minix10groupbillede.png)
![](minix10groupbillede2.png)

FLOWCHARTS
The overall concept:
We think that exploring sexsuality and how one can misuse data (intentionally or unintentionally). We like to explore the fact that nothing is neutral, even though one might think it is and how we put our trust into institutions sometimes without being critical. Both of our problems examine these issues. We want to extract some kind of data from the internet and use this data to criticize or clarify some of these problems or inequalities that are present in today’s society. 

Sex and Salary
This program makes the differences in salary depending on gender visible. 
To start with the program defines your gender based on a simplified face recognition technology. Based on your gender the program will then show what your average salary should be. This is based on numbers from Danmarks Statistik (dst.dk). If you’re a woman the program will tell you that you could get a much higher salary being a man - and it then refers you to a website with information about gender change. 
Selecting features in machine learning can be political, even though it is automatic. (Features are what parameters that are measured and used in the model - e.g. what constitutes a female or male face). The issues of constructing features - especially unsupervised clustering of data and automatic feature selection can be hard to control. 
Some of the technical challenges that we might face is regarding feature selection. How should the program be able to determine what features on the face related to women, and what features relates to men. The problem is that feature selection is always subjective in the sense that it collects significant data. There are therefore some ethical issues connected to that. The coherence between the significant data and reality, but one also has to be aware that the coherence might be “wrong”. One also has to be aware that it is not this exact coherence one wants to implement into the program. 
One of the ways to address this problem is that we have to be very aware of how the models have been built and what data has been collected/used. Be aware if the models are transparent. 
We deliberately chose only two genders, even though we are aware that there are over 60 different genders. 

Sexuality and Rights
What we wanted to criticize with this program is the huge inequality that is between different sexualities. Also the limiting binary choice of hetero or not, as a problematic convension and norm that still needs attention around the world. 
It is also a comment on a caricatured view on a heteronormative relationship; a view that is still very much present and “locks” people in certain roles based on expectations to their gender. 
This program is meant to tell a story about one's life based on different data inputs which the subject gives the program and the program will then tell you a fairytale about your life based on different parameters which are changeable. 
This program makes you choose your sexuality (whether or not you are hetero) and tells you a fairytale based on your input. The fairytale will be affected by the sexality and other info you give to the program.
You are only giving the choice of being hetorosexual or not. So anything but hetoresexual will be treated in another way. If your heterosexual you are presented with a straight forward, heteronormative story about your life - and if you’re anything else you will be presented with all kinds of challenges and misery (based on real conditions about unequal rights, repression and so on). 
These different endings also show how one’s life has a different outcome based on sexuality. 
One of the difficulties that could occur is collecting data from the different countries and their laws about sexuality and putting it all into our own JSON-file. We also have to figure out if it makes more sense to have many different JSON-files in order to better organize our data or to put it in fewer JSON-files. 
