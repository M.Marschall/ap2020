
let arrayOfDots = [];
let selectedDots = [];
let howManyDots;
let exifData = [];
let dataLabels = [];
let dataValues= [];
let img;



function setup() {
  createCanvas(windowWidth, windowHeight-100);
  input = createFileInput(handleFile);
  input.position(0, 0);



}

function draw() {

  //This draws the line from dot to dot - selectedDots is an array with all the choosen dots.
  //Every time you click it will add x and y of that dot to the array
  if(selectedDots.length>1) {
    for(let i = 0; i < selectedDots.length; i++) {
      if(i<1) {
        line(selectedDots[0][0], selectedDots[0][1],selectedDots[i][0], selectedDots[i][1]);
        textSize(10);
        text(exifData[0][0]+ ": " + exifData[0][1], selectedDots[i][0]+20, selectedDots[i][1]-20);
      }

      if(i>0) {
          line(selectedDots[i-1][0], selectedDots[i-1][1],selectedDots[i][0], selectedDots[i][1]);
          textSize(10);
          text(exifData[i][0]+ ": " + exifData[i][1], selectedDots[i][0]+20, selectedDots[i][1]-20);
      }


    }
  }

}
//Handles th input file
function handleFile(file) {
  //Checks if it actually is a image file that has been uploaded
  if (file.type === 'image') {
    //i pass the checkExif as a callback, because then i am sure that the image load will be finished before it conteniues
    img = createImg(file.data, '', 'anonymous', checkExif);
    img.hide();
  } else {
    console.log('uploaded file must be an image');
  }
}

function checkExif() {
  // Here i am using the EXIF.js to get all the exif information
  EXIF.getData(img.elt, function() {
    console.log(EXIF.getAllTags(this));
    dataLabels = Object.keys(EXIF.getAllTags(this));
    dataValues = Object.values(EXIF.getAllTags(this));
    for(let i = 0; i < dataLabels.length;i++){
        exifData.push([dataLabels[i], dataValues[i]]);
    }
    addNavigatorDataPoints();
    classifyImage();
    howManyDots = exifData.length;
    //Here i draw the dots based on the amount of data points in the exif metadata
    if(exifData.length>=0){
      for (let i = 0; i < howManyDots; i++) {
        arrayOfDots.push(new dots());
      }
      for (let i = 0; i < arrayOfDots.length; i++) {
        arrayOfDots[i].display();
      }
    }
  });
}
function addNavigatorDataPoints() {
  //Get available ram - can display a maximum of 8
  const memory = navigator.deviceMemory;
    exifData.push(["This device has a least", memory + "GiB of RAM"]);

    //get the language of the browser
    exifData.push(["Your preffered languages is ", navigator.language]);

    // this section gets the amount of processing threads available on the users computer
    // Usually every core can run more threads (often 2), which means that to get the actual number of physical cores you will have to divide the number by 2

    exifData.push(["Your computer has this many cores: ", window.navigator.hardwareConcurrency/2])


}

function mousePressed() {
  //On click the program checks if any of the dots are at the same location as the mouse - take a look at clickedDot() for more information

  for(let i = 0; i < arrayOfDots.length; i++) {
    if(arrayOfDots[i].clickedDot()) {
      //print("Check"); // use these checks to debug the program
      selectedDots.push([arrayOfDots[i].x, arrayOfDots[i].y]);
    }
  }

}

//This class is the to create the dots around the canvas while passing the individual dots to an array

class dots {
  constructor() {
    this.x = random(100,width-100);
    this.y = random(100,height-100);
    this.diameter = random(15, 20);
    //This is to make shure that you cannnot click the same dot twice :)
    this.isAlreadyClicked = false;
  }
  //method that draws the ellipse
  display() {
    ellipse(this.x, this.y, this.diameter, this.diameter);
  }
  //This method handles what happens when you click a dot
  clickedDot() {
      //Here i just get the distance from the mouse to the center of the ellipse
        var d = dist(this.x, this.y, mouseX, mouseY);
        //If the distance is shorter than half the diameter(radius) of the ellipse then the mouse must be on top of it
        if (d < this.diameter/2 && this.isAlreadyClicked == false) {
          //marks the dot as clicked
            this.isAlreadyClicked = true;
            //returns true so that i can use the method as a condition
            return true;
        } else {
            return false;
        }
    }
}


//This is a image classification model named MobileNet, which is a fairly small but fast model.
//I did not have time to implement it, but i thought it i would leave it here to give an example of how much information an image can carry
// this prints the detected objects out in the console. I does not work that well, and the model is fairly basic
// but what you should consider is how a powerful model like the one that is implemented in google vision can detect objects, products and animals
//So imagine that you would take a picture of a pair of shoes - when that picture is uploaded the reciever would be able
// to get locations, time and what you were looking at. Check out Google Cloud Vision here https://cloud.google.com/vision/docs/drag-and-drop
  // Load the model.
function classifyImage(){
  mobilenet.load().then(model => {
    model.classify(img.elt).then(predictions => {
      console.log('Predictions: ');
      console.log(predictions);
      console.log(Object.keys(predictions));
    });
  });


}





///___________________________This is not in use_____________________________

////////////////////////////////////////////////////////////////////////////////
//Getting the location of the user
//First i am creating an object which contains the options for the get getCurrentPosition();
//let posOptions = {
  //enableHighAccuracy: true, //this basically just sees if it has some better options than the ip to determine the location - eq f the device has GPS buit in

  //timeout: 5000, //If it cant get the location within 5 seconds - it should stop trying
//};


/*

Here i am trying to get the location of the user ----
I have put in 2 arguments, and both of them are functions used as Callbacks.

The way it works is that callback functions are called when the method/function is finished,
which in this case means that when the greCurrentPosition() is finished it has 2
possible outcomes - etiher it has succeded in getting the location of the user
and it will call the logInfo()

or it has failed and will call the noLocationError();


navigator.geolocation.getCurrentPosition(logInfo, noLocationError, posOptions);

function logInfo(position) {
  // Show a map centered at (position.coords.latitude, position.coords.longitude).
  console.log(position.coords.latitude);
}

function noLocationError() {
  console.log("no location was found");
}

*/
////////////////////////////////////////////////////////////////////////////////
