# MiniEx4
Click! That is the sound of your phone taking the picture of a nice pair of shoes you want to buy or a picture of your dinner at a resturant. But not all of the picture is captured by an image sensor. When taking an image with your smartphone you are adding around 55 extra data points like the location, shooting angle, type of shot, speed.

I decided to create actual data "points" or dots that would appear randomly on the canvas. All the Exif metadata from an image is read and creates a dot for each data point. When you run the program you will have to upload and image and then you are the algorithm connecting the dots. In itself, it is really basic but when you go through the data points, you will see that the data is very precise. Exact location, speed and direction, and all settings are stored when you capture an image with your phone. Imagine how much can be drawn from this when a picture is uploaded and if we add the features the advanced object and image classification we can find out what shoes you want to buy or we would like you to buy and your supposedly favorite restaurant.
https://m.marschall.gitlab.io/-/ap2020/-/jobs/455536500/artifacts/public/AllMiniEx/MiniEx4/index.html

# These are notes I wrote during the research

## Read this after trying out the program - Thoughts in The process

So this time i am making the readme a little different - I am going to write this while I am figuring out my MiniEx4. I started out b asking the following questions:

___what I can get to no about a user just with one page visit? (without the use of third party analytics)___

___What is possible to do with this information?____

To go about answer these questions, I started out by checking the ```window.navigator``` API to see what information about the user I could get from that.
https://developer.mozilla.org/en-US/docs/Web/API/Navigator  

I went trough all of the methods one by one to find the ones I thought was returning the most interesting information. To see the both the structure of the information and the information itself, I used the following code to print and examine the returned values.
```Javascript
let infoAboutUser = navigator.connection;

console.log(infoAboutUser);

```
I just exchanged ```navigator.connection``` with other methods on the list to see the result of each method. Of course that did not work on all of them, because some of the method required 1 or more arguments, but then I just had to read further into the documentation.

#### A detour into the W3 javascript development

Going through all the method and into the raised a lot of questions about how fast technological development, and how little I personally know about it. To be honest I do not know where to start, so I just want to share this webpages with you: https://www.w3.org/das/roadmap

This site provides an overview of the roadmap of what sensor, trackers and other data-capturing APIs are implemented or under development in JavaScript.

What I find a little concerning about these projects is the "Security and privacy considerations" section under each project. In general it describes the precautions and instructions that should be considered by the user agent. The definition of a user agent is "___A user agent is any software that retrieves, renders and facilitates end user interaction with Web content___" (https://www.w3.org/WAI/UA/work/wiki/Definition_of_User_Agent ), like Google Chrome, Firefox, Opera etc. But what if they do not consider these things? We absolutely rely on these tools to be secure or else we would have no idea what data a website could retrieve from us. Combined with the growing list of sensors that contribute to this datafication of users, it is more important than ever that these security considerations keep  with technological development.
#### Exif Metadata - how much data does an image carry

I did not want to go to much into the different sensors, but I was thinking about how I could retrieve more information about the user. Through working a lot with video and pictures, I have worked with the metadata of an image. A few google searches and an app later I got this:
```
(file) DateTime: 2020:02:28 10:02:46
Make: Apple
Model: iPhone 6s
Orientation: Up
ResolutionUnit: Inches
Software: 13.3.1
XResolution: 72
YResolution: 72
ApertureValue: 2.275007047547454
BrightnessValue: 4.823516435031988
ColorSpace: sRGB
ComponentsConfiguration: YCbCr
CustomRendered: HDR (Composite)
DateTimeDigitized: 2020:02:28 10:02:46
DateTimeOriginal: 2020:02:28 10:02:46
ExifVersion: 2.3.1
ExposureBiasValue: 0
ExposureMode: Auto
ExposureProgram: Normal
ExposureTime: 0.02
FNumber: 2.2
Flash: Auto, Did not fire
FlashPixVersion: 1.0
FocalLenIn35mmFilm: 29
FocalLength: 4.15
ISOSpeedRatings: 25
LensMake: Apple
LensModel: iPhone 6s back camera 4.15mm f/2.2
LensSpecification: 4.15,4.15,2.2,2.2
MeteringMode: Spot
OffsetTime: +01:00
OffsetTimeDigitized: +01:00
OffsetTimeOriginal: +01:00
PixelXDimension: 4032
PixelYDimension: 3024
SceneCaptureType: Standard
SceneType: Directly Photographed
SensingMethod: One-chip color area
ShutterSpeedValue: 5.644144754676064
SubjectArea:
X Center: 1000,
Y Center: 1766,
Width: 753,
Height: 756
SubsecTimeDigitized: 924
SubsecTimeOriginal: 924
WhiteBalance: Auto
Altitude: 3.462719917811738m
AltitudeRef: Above sea level
DestBearing: 250.9693450998606
DestBearingRef: T
HPositioningError: 65
ImgDirection: 250.9693°= West
ImgDirectionRef: True North
Latitude: 56.14881383333334
LatitudeRef: N
Longitude: 10.20796333333333
LongitudeRef: E
Speed: 0
SpeedRef: km/h
PixelWidth: 4032
PixelHeight: 3024
FileName: IMG_0575.JPG
FileSize: 2.26493 MB

````
So this is all the data that a single photo from my iPhone carries.
I decided to if I am able to import all these data points a write a small Online profile an that basis.

I tried to setup image classification with MobileNet model and tensorflow, but I ran out of time.
Instead I would recommened trying out the Google Cloud Vision - it is pretty cool  
https://cloud.google.com/vision/docs/drag-and-drop
