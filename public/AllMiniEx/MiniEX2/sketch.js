let video;
// let interval;
let exprs = {};
let face;
let currentEmotion = 'neutral';

let indexOfMostProbableEmotion = 0;
let possibleEmotions = [];

function setup() {

  createCanvas(640, 480);
  background(255);

  video = createCapture(VIDEO);
  video.hide(); // hides the html video element i just created
  // interval = setInterval(timeIt, 100); // calls the funktion timeIt every 100ms.

  //loading the face-api.js using ml5.js (machine learning Library for p5.js)
  faceapi.load
  faceapi.loadSsdMobilenetv1Model('./models'); //loads the models for finding a face
  faceapi.loadFaceExpressionModel('./models'); //loads the models for determining face expressions.
}


//i use async function to make sure that the facial detection can be finished before proceeding.
async function mousePressed() {


  const myPromise = async () => {
    //i tell the program to wait for the function to finish
    await faceapi.detectSingleFace(video.elt).withFaceExpressions()
      .then((detectionOfExpression) => {

        //this just checks if there is even a face in the picture ;-)
        if (detectionOfExpression == undefined) {
          console.log("No face detected");
        } else {
          face = detectionOfExpression.detection;
          exprs = detectionOfExpression.expressions;
          print(exprs);
          //i get the index number of the larges value
          indexOfMostProbableEmotion = Object.values(exprs).indexOf(Math.max(...Object.values(exprs)));
          possibleEmotions = Object.keys(exprs);
          //Object.values returns an array[] and max() just returns
          //console.log(Math.max(Object.values(exprs)));

        }

      });

////////////////////////////////////////////////////////////////////
////////////////////This sections is not in use/////////////////////
////////////////////////////////////////////////////////////////////


      /*
console.log(possibleEmotions);
      console.log(possibleEmotions[indexOfMostProbableEmotion]);


      if (Object.values(exprs)[indexOfMostProbableEmotion] >= 0.5) {
          currentEmotion = possibleEmotions[indexOfMostProbableEmotion];
          console.log(currentEmotion);

      }
      else {
        currentEmotion = 'neutral';
      }*/
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////


  }
  myPromise();


}

function draw() {

  background(255, 0, 0);
  image(video, 0, 0, 640, 480); //puts the video onto the canvas
  scale(1);

  if(indexOfMostProbableEmotion === 1) {
    happyFace(200,300);
  }
  if(indexOfMostProbableEmotion === 3) {
    angryFace(200,300);
  }
  if(indexOfMostProbableEmotion === 6) {
    suprisedFace(200,300);
  }
  if (indexOfMostProbableEmotion === 0) {

    neutralFace(200,300);
  }
  if (indexOfMostProbableEmotion ===5) {
    fill(0, 102, 153);
    textSize(30);
    text('disgusted?', 200, 300);
  }
  if (indexOfMostProbableEmotion === 2) {
    fill(0, 102, 153);
    textSize(30);
    text('sad?', 200, 300);
  }

}
//These functions are just for drawing the different emojies
function happyFace(x, y) {
  let facePositionX = x;
  let facePositionY = y;
  let defaultSizeOfFace = 100;
  noStroke();
  fill(0, 255, 255);
  ellipse(facePositionX, facePositionY, defaultSizeOfFace, defaultSizeOfFace);

  stroke(231, 62, 141);
  strokeWeight(defaultSizeOfFace / 15);
  let mouthOffsetY = defaultSizeOfFace / 10;
  let smileSize = facePositionY + defaultSizeOfFace / 2.5;
  let cornerOfMouthL = facePositionX - defaultSizeOfFace / 3;
  let cornerOfMouthR = facePositionX + defaultSizeOfFace / 3;
  let shapeMouth = 5;
  bezier(cornerOfMouthR, facePositionY + mouthOffsetY, cornerOfMouthR - shapeMouth, smileSize, cornerOfMouthL + shapeMouth, smileSize, cornerOfMouthL, facePositionY + mouthOffsetY);

  let eyeWidth = 10;
  let eyeHeight = 15;
  let eyePlacementXfromCenter = defaultSizeOfFace / 7;
  let eyePlacementYfromCenter = defaultSizeOfFace / 7;
  ellipse(facePositionX - eyePlacementXfromCenter, facePositionY - eyePlacementYfromCenter, eyeWidth, eyeHeight);
  ellipse(facePositionX + eyePlacementXfromCenter, facePositionY - eyePlacementYfromCenter, eyeWidth, eyeHeight);

}

function angryFace(x, y) {
  let facePositionX = x;
  let facePositionY = y;
  let defaultSizeOfFace = 100;
  noStroke();
  fill(0, 255, 255);
  ellipse(facePositionX, facePositionY, defaultSizeOfFace, defaultSizeOfFace);
  stroke(231, 62, 141);
  strokeWeight(defaultSizeOfFace / 15);
  let mouthOffsetY = defaultSizeOfFace / 4;
  let smileSize = facePositionY - defaultSizeOfFace / 5;
  let cornerOfMouthL = facePositionX - defaultSizeOfFace / 4;
  let cornerOfMouthR = facePositionX + defaultSizeOfFace / 4;
  let shapeMouth = 5;
  bezier(cornerOfMouthR, facePositionY + mouthOffsetY, cornerOfMouthR - shapeMouth, smileSize + mouthOffsetY, cornerOfMouthL + shapeMouth, smileSize + mouthOffsetY, cornerOfMouthL, facePositionY + mouthOffsetY);

  let eyeWidth = 10;
  let eyeHeight = 15;
  let eyePlacementXfromCenter = defaultSizeOfFace / 7;
  let eyePlacementYfromCenter = defaultSizeOfFace / 7;
  ellipse(facePositionX - eyePlacementXfromCenter, facePositionY - eyePlacementYfromCenter, eyeWidth, eyeHeight);
  ellipse(facePositionX + eyePlacementXfromCenter, facePositionY - eyePlacementYfromCenter, eyeWidth, eyeHeight);
}

function suprisedFace(x, y) {
  let facePositionX = x;
  let facePositionY = y;
  let defaultSizeOfFace = 100;
  noStroke();
  fill(0, 255, 255);
  ellipse(facePositionX, facePositionY, defaultSizeOfFace, defaultSizeOfFace);
  let mouthPositionX = facePositionX;
  let mouthPositionY = facePositionY + 22;
  fill(231, 62, 141);
  ellipse(mouthPositionX, mouthPositionY, defaultSizeOfFace / 3, defaultSizeOfFace / 2.5);

  let eyeWidth = 10;
  let eyeHeight = 15;
  let eyePlacementXfromCenter = defaultSizeOfFace / 7;
  let eyePlacementYfromCenter = defaultSizeOfFace / 7;
  noFill();
  stroke(231, 62, 141);
  strokeWeight(defaultSizeOfFace / 15);
  ellipse(facePositionX - eyePlacementXfromCenter, facePositionY - eyePlacementYfromCenter, eyeWidth, eyeHeight);
  ellipse(facePositionX + eyePlacementXfromCenter, facePositionY - eyePlacementYfromCenter, eyeWidth, eyeHeight);
}

function neutralFace(x, y) {
  let facePositionX = x;
  let facePositionY = y;
  let defaultSizeOfFace = 100;
  noStroke();
  fill(0, 255, 255);
  ellipse(facePositionX, facePositionY, defaultSizeOfFace, defaultSizeOfFace);
  stroke(231, 62, 141);
  strokeWeight(defaultSizeOfFace / 15);
  let mouthOffsetY = facePositionY + defaultSizeOfFace / 5;
  let cornerOfMouthL = facePositionX - defaultSizeOfFace / 4;
  let cornerOfMouthR = facePositionX + defaultSizeOfFace / 4;
  line(cornerOfMouthR, mouthOffsetY, cornerOfMouthL, mouthOffsetY);

  let eyeWidth = 10;
  let eyeHeight = 15;
  let eyePlacementXfromCenter = defaultSizeOfFace / 7;
  let eyePlacementYfromCenter = defaultSizeOfFace / 7;
  ellipse(facePositionX - eyePlacementXfromCenter, facePositionY - eyePlacementYfromCenter, eyeWidth, eyeHeight);
  ellipse(facePositionX + eyePlacementXfromCenter, facePositionY - eyePlacementYfromCenter, eyeWidth, eyeHeight);
}




//this funktion is using the interval variable to determine at what rate this funktion should be called.
/*
function timeIt() {
  //detectMyEmotions();
}*/
