![Screenshot1](assets/markdown-img-paste-20200217021006334.png)

So in this i challenged myself with some facial recognitions by using a library called face-api.js. [Click here for GitHub](https://github.com/justadudewhohacks/face-api.js/)
It is build around the machine learning library [tensorflow.js](https://www.tensorflow.org/js/)

I made a program that chooses an emoji based on the facial expression of the user by utilizing the webcam. Every time you click the mousepad i will update and try to choose the correct emoji. It was hard to get it working with p5.js but i succeeded after a lot of trails and errors.

I though it would be fun to create a program that forces you to mimic the emoji that you want to use. especially because they are always so overly expressive. They are always extremely happy or very sad. It is like a polarization of emotions and it is almost expected that we use em to express how we feel, even though we might not feel a lot towards certain topics.

The emojis i have been drawing are very simple and does not really resemble real human beings

https://m.marschall.gitlab.io/-/ap2020/-/jobs/439158112/artifacts/public/AllMiniEx/MiniEX2/index.html
