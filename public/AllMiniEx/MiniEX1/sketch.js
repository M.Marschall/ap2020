//3D models variables
let earth;
let as;
let cube;

//Variables for switching the models
let modelCounter = 0;
let currentModel;

//rotation of the model displayed
let currentRotationX = 100;
let currentRotationY = 100;

//for the text at the bottom f the page (i am not using the someFont variable)
let someFont;
let myDiv;

function preload() {

  //Here i am loading the 3D model - the preload() function ensures that it will load before executing
  earth = loadModel('assets/earth.obj', true);
  as = loadModel('assets/as.obj', true);
  cube = loadModel('assets/cube.obj', true);

  //I start out by setting the earth 3d mode as the default
  currentModel = earth;
  someFont = loadFont('assets/MontserratAlternates-Bold.otf');
}

function setup() {

  /*setting up the canvas by getting the screen width and height
   -- i subtract 200 pixels to ensure that everything can be displayed
   */
  createCanvas(windowWidth, windowHeight -=200, WEBGL );

  myDiv = createDiv('Current mouse position: ' + mouseX + ',' + mouseY);

}

//if "space" is pressed then it will trigger this function and switch to the the next model
function keyTyped() {
  
  if (key === " ") {
    print(modelCounter)
    if (modelCounter === 0) {
      currentModel = earth;
      print("earth");

    }
    if (modelCounter === 1) {
      currentModel = as;
      print("as");

    }
    if (modelCounter === 2) {
      currentModel = cube;
      print("cube");

    }

    //Resets the counter so that it loops
    if (modelCounter >= 2) {
      modelCounter = -1;
    }

  }
}

//on release of the space key it will add 1 to the modelCounter variable
function keyReleased() {
  if (key === " ") {
    modelCounter += 1;
  }
}
function draw() {
    //Orbs camera around model on click and hold
    orbitControl(10,10,10);

  //Always sets the location of a single spotlight to the mouse position
   let locXLight = mouseX - height / 2;
   let locYLight = mouseY - width / 2;
   pointLight(200, 200, 200, locXLight, locYLight, 100);

  //static lights
  directionalLight(0, 128, 128, -50, 0.25, 50);
  directionalLight(255, 156, 0, 50, -0.25, 0);
  ambientLight(0,0,0,40);

  // Scaling the 3D model
  scale(2.4);

  //Rotating model with arrowkeys
  if (keyIsDown(LEFT_ARROW)) {
    currentRotationY += 5;
  }

  if (keyIsDown(RIGHT_ARROW)) {
    currentRotationY -= 5;
  }

  if (keyIsDown(UP_ARROW)) {
    currentRotationX += 5;
  }

  if (keyIsDown(DOWN_ARROW)) {
    currentRotationX -= 5;
  }
    rotateX(radians(currentRotationX));
    rotateY(radians(currentRotationY));

  //addig the last bit of text to the page
  myDiv.html('Current mouse position: ' + round(mouseX) + ',' + round(mouseY) + '  ---------  You can use the arrow keys to rotate the 3d object and the mouse wil orbit around the object. To go to nexr object press SPACE');
 // Setting the material of the 3D Model
   ambientMaterial(250);
   clear();
   model(currentModel);

}
