![Screenshot](ScreenShort.png)

I have created a little program where you are able to switch between 3 different 3d objects by pressing enter. Further more is it possible to rotate the object with the arrow keys and also to orbit around the object with the mouse.
### How would you describe your first independent coding experience?
---
<p>I think P5 seem fun and intuitive but I do make a lot of dumb small errors that takes forever to find and are really obvious when you see them. I would really like to combine it with som other libraries since there are so much cool stuff out there :) </p>

### How is the coding process different from, or similar to, reading and writing text?
---
<p>It is somehow more visible compared to writing I believe. I really enjoy that you get a direct "feedback" when you have written something, and then you run it. It is a very flexible medium. Both writing and reading text and code is very context dependent and it's meaning can chance accordingly.

### What does code and programming mean to you, and how do the assigned readings help you to reflect on programming?
---
<p> For my it is a maker tool. I do also believe that it is important to understand how it works, to fully grasp what influence it has on our culture and daly lives but foremost it is a vital tool for making digital products and design and therefore also important for my future career. </p>

https://m.marschall.gitlab.io/-/ap2020/-/jobs/431174748/artifacts/public/AllMiniEx/MiniEX1/index.html